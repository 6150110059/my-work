package com.programming.android.listviews;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Define the movies’ list items as an array of strings
        String[] Movies = new String[]{
                "Batman vs Superman",
                "13 Hours", "Deadpool",
                "The Forest ",
                "The 5th Wave",
                "10 Cloverfield Lane",
                "Free State of Jones",
                "Triple 9",
                "Hail, Caesar!",
                "Captain America"
        };
// Bind the list view with the layout
        final ListView listViewMovie = (ListView) findViewById(R.id.listViewMovie);
// Connect the list view to arrays with an array adapter
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, Movies);
        listViewMovie.setAdapter(adapter);

        listViewMovie.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String selected = (String) ((TextView) view).getText();

                Toast.makeText(getApplicationContext(), selected,
                        Toast.LENGTH_SHORT).show();

                //For opening the web browser.
                if (position == 0) {
                    Intent webIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://batmanvsuperman.dccomics.com/"));
                    startActivity(webIntent);
                }
//For making the phone call.
                if (position == 1) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:191"));
                    startActivity(callIntent);
                }

            }
        });
    }
}