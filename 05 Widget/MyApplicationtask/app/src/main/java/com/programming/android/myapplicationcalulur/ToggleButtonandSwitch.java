package com.programming.android.myapplicationcalulur;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

public class ToggleButtonandSwitch extends AppCompatActivity {

    private ToggleButton tgb1, tgb2;
    private Button btn;
    private Switch sw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.togglebuttonandswitch);

        tgb1 = (ToggleButton) findViewById(R.id.toggleButton1);
        tgb2 = (ToggleButton) findViewById(R.id.toggleButton2);
        btn = (Button) findViewById(R.id.btnDisplay);
        sw = (Switch) findViewById(R.id.switch1);
        tgb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean
                    isChecked) {
                if (isChecked) {
                    Toast.makeText(getApplicationContext(), "Toggle Button-1 is ON",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Toggle Button-1 is OFF",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean
                    isChecked) {
                if (isChecked) {
                    Toast.makeText(getApplicationContext(), "Switch is currently ON",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Switch is currently OFF", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuffer result = new StringBuffer();
                result.append("Toggle Button-1 : ").append(tgb1.getText());
                result.append("\nToggle Button-2 : ").append(tgb2.getText());
                result.append("\nSwitch : ").append(sw.isChecked());
                Toast.makeText(ToggleButtonandSwitch.this, result.toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });

    }
}