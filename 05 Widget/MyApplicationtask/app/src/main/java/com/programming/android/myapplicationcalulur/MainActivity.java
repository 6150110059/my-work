package com.programming.android.myapplicationcalulur;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn = findViewById(R.id.btncal);
    }

    public void Next(View view){
        Intent intent = new Intent(MainActivity.this, Calculator.class);
        startActivity(intent);
    }

    public void Next2(View view){
        Intent intent = new Intent(MainActivity.this, CheckBox.class);
        startActivity(intent);
    }

    public void Next3(View view){
        Intent intent = new Intent(MainActivity.this, RadioButton.class);
        startActivity(intent);
    }

    public void Next4(View view){
        Intent intent = new Intent(MainActivity.this, ToggleButtonandSwitch.class);
        startActivity(intent);
    }

    public void Next5(View view){
        Intent intent = new Intent(MainActivity.this, Spinner.class);
        startActivity(intent);
    }

    public void Next6(View view){
        Intent intent = new Intent(MainActivity.this, RatingBar.class);
        startActivity(intent);
    }

    public void Next8(View view){
        Intent intent = new Intent(MainActivity.this, SeekBar.class);
        startActivity(intent);
    }

    public void Next9(View view){
        Intent intent = new Intent(MainActivity.this, ImageView.class);
        startActivity(intent);
    }
}