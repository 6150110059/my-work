package com.programming.android.myapplicationcalulur;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class RatingBar extends AppCompatActivity {

    android.widget.RatingBar ratingBar1;
    TextView textView1;
    Button button1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ratingbar);

        ratingBar1 = (android.widget.RatingBar) findViewById(R.id.ratingBar1);
        textView1 = (TextView) findViewById(R.id.textView1);
        button1 = (Button) findViewById(R.id.button1);
        ratingBar1.setOnRatingBarChangeListener(new android.widget.RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(android.widget.RatingBar ratingBar, float rating, boolean fromUser) {
                textView1.setText("Rating : " +String.valueOf(rating));
            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(RatingBar.this, "Your rating : " + String.valueOf(ratingBar1.getRating()), Toast.LENGTH_LONG).show();
            }
        });

    }
}