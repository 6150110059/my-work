package com.programming.android.myapplicationcalulur;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class SeekBar extends AppCompatActivity {

    private android.widget.SeekBar seekBar1;
    private TextView textView1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.seekbar);

        seekBar1 = (android.widget.SeekBar) findViewById(R.id.seekBar1);
        textView1 = (TextView) findViewById(R.id.textView1);
        textView1.setText("Value : " + seekBar1.getProgress() + "/" +
                seekBar1.getMax());
        seekBar1.setOnSeekBarChangeListener(new android.widget.SeekBar.OnSeekBarChangeListener() {
            int progressvalue = 0;
            @Override
            public void onProgressChanged(android.widget.SeekBar seekBar, int progress, boolean
                    fromUser) {
                progressvalue = progress;
            }
            @Override
            public void onStartTrackingTouch(android.widget.SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(android.widget.SeekBar seekBar) {
                textView1.setText("Value : " + progressvalue + "/" +
                        seekBar.getMax());
                Toast.makeText(getApplicationContext(), "Stopped tracking seekbar",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

}