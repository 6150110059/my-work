package com.example.retrofitandroidlistviewexample;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ImageAPI {

    public static final String BASE_URL = "http://172.20.145.241/retrofit/";


    @GET("list_images.php")
    Call<List<ImagesList>> getImages();
}
