package com.programming.android.myapplicationas1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.ClientCertRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivityWeb extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_web);

        WebView web = findViewById(R.id.web);
        web.setWebViewClient(new WebViewClient());
        web.loadUrl("https://www.google.com");
    }
}