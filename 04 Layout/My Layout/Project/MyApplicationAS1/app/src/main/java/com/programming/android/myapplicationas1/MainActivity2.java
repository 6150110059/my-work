package com.programming.android.myapplicationas1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

    }
    public void nextabout (View view){
        Button btnabout = findViewById(R.id.btnabout);
        Intent intent = new Intent(MainActivity2.this,About.class);
        startActivity(intent);
    }

    public void nextweb (View view){
        Button btnweb = findViewById(R.id.btnweb);
        Intent intentweb = new Intent(MainActivity2.this,MainActivityWeb.class);
        startActivity(intentweb);
    }

    public void exit (View view){
        finish();

    }
}